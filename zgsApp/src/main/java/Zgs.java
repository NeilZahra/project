

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Zgs {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    public static void connectAndGetAll()
    {
        try
        {
            //connect
            Connection con = DriverManager.getConnection
               ("jdbc:mysql://localhost:3307/zgs_db", "root", "");
            
            //make a query
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM employees_tbl");
            //ResultSet rs = stmt.executeQuery("Select * from products");
            
            //output result
            while (rs.next())
            {
                System.out.println(rs.getString(1) +"\t" +
                                   rs.getString(2) +"\t" +
                                   rs.getInt(3));
            }
            
            //close connection
            con.close();            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
